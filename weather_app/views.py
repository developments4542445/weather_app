from django.shortcuts import render


def home(request):
    import json
    import requests

    if request.method == "POST":
        zipcode = request.POST['zipcode']
        api_request = requests.get("https://www.airnowapi.org/aq/observation/zipCode/current/?format=application/"
                                   "json&zipCode=" + zipcode + "&distance=5&API_KEY=C1F0BCC3-9B3C-"
                                                               "4F82-87F7-5C16C411879B")
        try:
            api = json.loads(api_request.content)

        except (Exception,) as _:
            api = "Error"

        if api[0]['Category']['Name'] == "Good":
            category_description = ("(0 - 50) Air quality is considered satisfactory, and air pollution poses little "
                                    "or no risk.")
            category_color = "good"
        elif api[0]['Category']['Name'] == "Moderate":
            category_description = "(51-100) Air quality is acceptable."
            category_color = "moderate"
        elif api[0]['Category']['Name'] == "Unhealthy for Sensitive Groups":
            category_description = ("(101-150) Although general public is not likely to be affected at this AQI range, "
                                    "people with lung disease, older adults and children are at a greater risk from "
                                    "exposure to ozone.")
            category_color = "usg"
        elif api[0]['Category']['Name'] == "Unhealthy":
            category_description = "(151-200) Everyone may begin to experience health effects."
            category_color = "unhealthy"
        elif api[0]['Category']['Name'] == "Very Unhealthy":
            category_description = "(201-300) Health alert."
            category_color = "veryunhealthy"
        elif api[0]['Category']['Name'] == "Hazardous":
            category_description = "(301-500) Health warnings of emergency conditions."
            category_color = "hazardous"
        else:
            category_description = "empty"
            category_color = "bg-light"

        return render(request, 'weather_app/home.html', {
            "api": api,
            "category_description": category_description,
            "category_color": category_color
        })
    else:
        api_request = requests.get("https://www.airnowapi.org/aq/observation/zipCode/current/?format=application/"
                                   "json&zipCode=89129&distance=5&API_KEY=C1F0BCC3-9B3C-4F82-87F7-5C16C411879B")
        try:
            api = json.loads(api_request.content)
        except (Exception,) as _:
            api = "Error"

        if api[0]['Category']['Name'] == "Good":
            category_description = ("(0 - 50) Air quality is considered satisfactory, and air pollution poses little "
                                    "or no risk.")
            category_color = "good"
        elif api[0]['Category']['Name'] == "Moderate":
            category_description = "(51-100) Air quality is acceptable."
            category_color = "moderate"
        elif api[0]['Category']['Name'] == "Unhealthy for Sensitive Groups":
            category_description = ("(101-150) Although general public is not likely to be affected at this AQI range, "
                                    "people with lung disease, older adults and children are at a greater risk from "
                                    "exposure to ozone.")
            category_color = "usg"
        elif api[0]['Category']['Name'] == "Unhealthy":
            category_description = "(151-200) Everyone may begin to experience health effects."
            category_color = "unhealthy"
        elif api[0]['Category']['Name'] == "Very Unhealthy":
            category_description = "(201-300) Health alert."
            category_color = "veryunhealthy"
        elif api[0]['Category']['Name'] == "Hazardous":
            category_description = "(301-500) Health warnings of emergency conditions."
            category_color = "hazardous"
        else:
            category_description = "empty"
            category_color = "bg-light"

        return render(request, 'weather_app/home.html', {
            "api": api,
            "category_description": category_description,
            "category_color": category_color
        })


def about(request):
    return render(request, 'weather_app/about.html', {})
