# Weather App

Easy and quick development where we can check weather in our place.

This project is based in:
- Python 3.10.9
- Django
- Third-party APIs

Libraries and others:
- Django==3.2.5
- Bootstrap: https://getbootstrap.com/
- Weather page: https://www.airnow.gov/
- Weather API page: https://docs.airnowapi.org/webservices
- Queries: https://docs.airnowapi.org/CurrentObservationsByZip/query

Steps procedure:
- Define quantity of apps
- Define and design quantity of html files to use
- Link all html files
- Include base file
- Include bootstrap CSS
- Include Django links
- Back-end code: API requests
- Beautify app with bootstrap
- Django forms